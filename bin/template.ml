open Tyxml.Html

let base content = html (head (title (txt "Datebridge")) []) (body content)

let homepage redirect_uri =
  base
    [
      a ~a:[ a_href (Uri.to_string redirect_uri) ] [ txt "Sign in using Raven" ];
    ]

let display data = base [ txt data ]

let events =
  base
    [
      ol
        [
          li [ txt "Event 1" ];
          li [ txt "Event 2" ];
          li [ txt "Event 3" ];
          li [ txt "Event 4" ];
        ];
    ]

let form csrf_tag action body =
  form ~a:[ a_method `Post; a_action action ] (Unsafe.data csrf_tag :: body)

let add_admin_form csrf_tag =
  form csrf_tag "add_admin"
    [
      label ~a:[ a_label_for "email" ] [ txt "Add admin:" ];
      br ();
      input ~a:[ a_name "email"; a_input_type `Email ] ();
      br ();
      input ~a:[ a_input_type `Submit ] ();
    ]

let create_event_form csrf_tag =
  form csrf_tag "create_event"
    [
      label
        ~a:[ a_label_for "datetime-local" ]
        (* The datetime is specified in the local timezone to avoid making
           timezone conversion and only ever displaying it in the timezone that
           matters to the user. *)
        [ txt "Date and time in local time at the time of the event: " ];
      br ();
      input
        ~a:
          [
            a_input_type `Datetime_local;
            a_id "datetime-local";
            a_name "datetime-local";
          ]
        ();
      br ();
      label ~a:[ a_label_for "location" ] [ txt "Location: " ];
      br ();
      input ~a:[ a_input_type `Text; a_id "location"; a_name "location" ] ();
      br ();
      label ~a:[ a_label_for "title" ] [ txt "Title: " ];
      br ();
      input ~a:[ a_input_type `Text; a_id "title"; a_name "title" ] ();
      br ();
      label ~a:[ a_label_for "description" ] [ txt "description: " ];
      br ();
      textarea ~a:[ a_id "description"; a_name "description" ] (txt "");
      br ();
      input ~a:[ a_input_type `Submit ] ();
    ]

let admin csrf_tag event_list admin_list =
  base
    [
      h2 [ txt "Admin management" ];
      h3 [ txt "List of events" ];
      form csrf_tag "remove_event"
        [
          ul
          @@ List.map
               (fun (event : Datebridge.Migration.event_with_id) ->
                 li
                   [
                     txt event.title;
                     txt " ";
                     button
                       ~a:
                         [
                           a_button_type `Submit;
                           a_name "event_id";
                           event.id |> string_of_int |> a_text_value;
                         ]
                       [ txt "remove event" ];
                   ])
               event_list;
        ];
      h3 [ txt "Create event" ];
      create_event_form csrf_tag;
      h3 [ txt "List of admins" ];
      form csrf_tag "remove_admin"
        [
          ul
          @@ List.map
               (fun email ->
                 li
                   [
                     txt email;
                     txt " ";
                     button
                       ~a:
                         [
                           a_button_type `Submit;
                           a_name "email";
                           a_text_value email;
                         ]
                       [ txt "remove as admin" ];
                   ])
               admin_list;
        ];
      add_admin_form csrf_tag;
    ]
