open Cohttp_lwt
open Cohttp_lwt_unix

let discovery_document_uri =
  Uri.of_string "https://accounts.google.com/.well-known/openid-configuration"

let client_id = Sys.getenv "CLIENT_ID"
let client_secret = Sys.getenv "CLIENT_SECRET"
let database_url = Sys.getenv "DATABASE_URL"

(* TODO: perhaps this shouldn't be hardcoded *)
let org = "cam.ac.uk"

(* TODO: maybe load from /proc/self/environ or infer from Host: HTTP header *)
let base_url = "http://localhost:8080"
let redirect_uri = base_url ^ "/oauth_callback"

let login_endpoint_uri =
  let uri =
    Uri.of_string
      "https://accounts.google.com/o/oauth2/v2/auth?response_type=code"
  in
  Uri.add_query_params' uri
    [
      ("client_id", client_id);
      ("hd", "cam.ac.uk");
      ("redirect_uri", redirect_uri);
      ("scope", "openid email" (*("scope", "profile openid");*));
    ]

let token_uri = Uri.of_string "https://oauth2.googleapis.com/token"
let html_to_string = Format.asprintf "%a" (Tyxml.Html.pp ())
let render_template html = html_to_string html |> Dream.html

let get_redirect_uri request =
  let (`Hex random) = Dream.random 32 |> Hex.of_string in
  let%map.Lwt () = Dream.set_session_field request "state" random in
  Uri.add_query_param' login_endpoint_uri ("state", random)

let email_field = Dream.new_field ()
let get_auth_email request = Dream.field request email_field |> Option.get

let () =
  Lwt_main.run
  @@
  let%lwt jwks_body =
    let%lwt _discovery_document_response, body =
      Client.get discovery_document_uri
    in
    let%lwt body = Body.to_string body in
    let%lwt _jwks_response, body =
      (Datebridge_j.openid_configuration_of_string body).jwks_uri
      |> Uri.of_string |> Client.get
    in
    Body.to_string body
  in
  let jwks = Jose.Jwks.of_string jwks_body in
  let get_jwt jwt_string =
    Result.bind (Jose.Jws.of_string jwt_string) (fun jws ->
        let jwt = Jose.Jwt.of_jws jws in
        Option.fold ~none:(Error `KidNotFound)
          ~some:(fun kid ->
            Option.fold ~none:(Error `JwkNotFound)
              ~some:(fun jwk ->
                Jose.Jwt.validate ~jwk ~now:(Ptime_clock.now ()) jwt)
              (Jose.Jwks.find_key jwks kid))
          jws.header.kid)
  in
  let auth_guard inner_handler request =
    match Dream.session_field request "id_token" with
    | Some jwt_string -> (
        match get_jwt jwt_string with
        | Ok jwt ->
            if Jose.Jwt.get_string_claim jwt "hd" |> Option.get = org then
              let () =
                Dream.set_field request email_field
                  (Jose.Jwt.get_string_claim jwt "email" |> Option.get)
              in
              inner_handler request
            else Dream.html "WRONG ORG"
        | Error `Expired ->
            let%lwt () =
              Dream.set_session_field request "redirect_uri"
                (Dream.target request)
            in
            let%lwt uri = get_redirect_uri request in
            Dream.redirect request (Uri.to_string uri)
        | Error _ -> failwith "unimplemented")
    | None -> Dream.html "NOT AUTHENTICATED"
  in
  let admin_guard inner_handler =
    auth_guard (fun request ->
        if%lwt
          Dream.sql request
            (Datebridge.Migration.is_admin (get_auth_email request))
        then inner_handler request
        else Dream.html "NOT AUTHENTICATED ADMIN PAGE TODO")
  in
  Dream.serve @@ Dream.logger
  @@ Dream.sql_pool database_url
  @@ Dream.sql_sessions (* ~lifetime:3600. *)
  @@ Dream.router
       [
         Dream.scope "/admin" [ admin_guard ]
           [
             Dream.get "/" (fun request ->
                 let%lwt admins =
                   Dream.sql request Datebridge.Migration.list_admins
                 and events =
                   Dream.sql request Datebridge.Migration.list_events
                 in
                 Template.admin (Dream.csrf_tag request) events admins
                 |> render_template);
             Dream.post "/remove_admin" (fun request ->
                 match%lwt Dream.form request with
                 | `Ok [ ("email", email) ] ->
                     if email = get_auth_email request then
                       Dream.html
                         "cannot remove yourself as admin otherwise there may \
                          be no admins left and the only way to add new admins \
                          is to go in the database and I can't be bothered to \
                          do that"
                     else
                       let%lwt () =
                         Dream.sql request
                           (Datebridge.Migration.remove_admin email)
                       in
                       Dream.redirect request "../admin/"
                 | _ -> Dream.empty `Bad_Request);
             Dream.post "/add_admin" (fun request ->
                 match%lwt Dream.form request with
                 | `Ok [ ("email", email) ] ->
                     let%lwt () =
                       Dream.sql request (Datebridge.Migration.add_admin email)
                     in
                     Dream.redirect request "../admin/"
                 | _ -> Dream.empty `Bad_Request);
             Dream.post "/create_event" (fun request ->
                 match%lwt Dream.form request with
                 | `Ok
                     [
                       ("datetime-local", datetime_local);
                       ("description", description);
                       ("location", location);
                       ("title", title);
                     ] ->
                     let%lwt () =
                       Dream.sql request
                         (Datebridge.Migration.add_event
                            { datetime_local; title; location; description })
                     in

                     Dream.redirect request "../admin/"
                 | _ -> Dream.empty `Bad_Request);
             Dream.post "/remove_event" (fun request ->
                 match%lwt Dream.form request with
                 | `Ok [ ("event_id", event_id) ] ->
                     let%lwt () =
                       Dream.sql request
                         (int_of_string event_id
                        |> Datebridge.Migration.remove_event)
                     in
                     Dream.redirect request "../admin/"
                 | _ -> Dream.empty `Bad_Request);
           ];
         (* TODO handle user refusing to give access *)
         Dream.get "/" (fun request ->
             match Dream.session_field request "id_token" with
             | Some jwt_string -> (
                 match get_jwt jwt_string with
                 | Ok _id -> Template.events |> render_template
                 | Error `Expired ->
                     let%lwt redirect_uri = get_redirect_uri request in
                     Dream.redirect request (Uri.to_string redirect_uri)
                 | Error `DifferentOrg -> failwith "org"
                 | Error (`Msg msg) -> failwith msg
                 | Error `Invalid_signature -> failwith "sig"
                 | Error _e -> failwith "unimplemented")
             | None ->
                 let%lwt redirect_uri = get_redirect_uri request in
                 Template.homepage redirect_uri |> render_template);
         Dream.get "/oauth_callback" (fun request ->
             let query_state = Dream.query request "state" in
             let session_state = Dream.session_field request "state" in
             match (query_state, session_state) with
             (* TODO: style nit - is if-then-else better? *)
             | Some query_state, Some session_state
               when query_state = session_state -> (
                 match Dream.query request "code" with
                 | Some code ->
                     let%lwt _response, body =
                       Client.post_form
                         ~params:
                           [
                             ("code", [ code ]);
                             ("client_id", [ client_id ]);
                             ("client_secret", [ client_secret ]);
                             ("redirect_uri", [ redirect_uri ]);
                             ("grant_type", [ "authorization_code" ]);
                           ]
                         token_uri
                     in
                     let%lwt body = Body.to_string body in
                     let token_response =
                       Datebridge_j.token_response_of_string body
                     in
                     let%lwt () =
                       Dream.set_session_field request "id_token"
                         token_response.id_token
                     in
                     let redirect_uri =
                       Dream.session_field request "redirect_uri"
                       |> Option.value ~default:"/"
                     in
                     let%lwt () =
                       Dream.drop_session_field request "redirect_uri"
                     in
                     Dream.redirect ~status:`Found request redirect_uri
                 | None -> Dream.html "TODO: no code")
             | _, _ ->
                 Dream.html
                   "TODO: handle invalid state - either let the user know or \
                    redirect them to login page");
       ]
