let database_url_env = "DATABASE_URL"

let () =
  match
    Lwt_main.run
    @@ Caqti_lwt.with_connection
         (Sys.getenv database_url_env |> Uri.of_string)
         (fun connection ->
           let%lwt () = Datebridge.Migration.upgrade_server connection in
           Lwt.return (Ok ()))
  with
  | Ok () -> ()
  | Error err -> failwith (Caqti_error.show err)
