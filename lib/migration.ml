module type DB = Caqti_lwt.CONNECTION

module T = Caqti_type

module Q = struct
  open Caqti_request.Infix

  let create_migrations_table_if_not_exist =
    (T.unit ->. T.unit)
      "CREATE TABLE IF NOT EXISTS _sql_migrations (\n\
      \  canOnlyHaveASingleRow boolean UNIQUE NOT NULL DEFAULT true,\n\
      \  version integer\n\
       )"

  let insert_default_version =
    (T.unit ->. T.unit)
      "INSERT INTO _sql_migrations(version) VALUES (0) ON CONFLICT \
       (canOnlyHaveASingleRow) DO NOTHING"

  let get_db_version =
    (T.unit ->! T.int64)
      "SELECT version FROM _sql_migrations ORDER BY version DESC LIMIT 1"

  let record_migration =
    (T.int ->. T.unit) "UPDATE _sql_migrations set version = ?"

  let is_admin =
    (T.string ->! T.int) "SELECT COUNT(*) FROM Admin WHERE email = ?"

  let list_admins = (T.unit ->* T.string) "SELECT email From Admin"
  let add_admin = (T.string ->. T.unit) "INSERT INTO Admin(email) VALUES (?)"
  let remove_admin = (T.string ->. T.unit) "DELETE FROM Admin WHERE email = ?"

  let add_event =
    (T.tup4 T.string T.string T.string T.string ->. T.unit)
      "INSERT INTO Event(datetimeLocal, title, location, description) VALUES \
       ($1::timestamp without time zone, $2, $3, $4)"

  let list_events =
    (T.unit ->* T.tup2 T.int (T.tup4 T.string T.string T.string T.string))
      "SELECT id, datetimeLocal, location, title, description FROM Event "

  let remove_event = (T.int ->. T.unit) "DELETE FROM Event WHERE id = ?"

  let migrations =
    [
      "CREATE TABLE Event (\n\
      \    id bigserial PRIMARY KEY,\n\
      \    datetime timestamp without time zone\n\
       )";
      "CREATE TABLE RavenUser (\n\
       id bigserial PRIMARY KEY,\n\
       access_token text NOT NULL\n\
       )";
      "CREATE TABLE dream_session (\n\
      \  id TEXT PRIMARY KEY,\n\
      \  label TEXT NOT NULL,\n\
      \  expires_at REAL NOT NULL,\n\
      \  payload TEXT NOT NULL\n\
       )";
      "CREATE TABLE Admin(email TEXT PRIMARY KEY)";
      "ALTER TABLE Event RENAME COLUMN datetime TO datetimeLocal";
      "ALTER TABLE Event ADD COLUMN title text not null, ADD COLUMN location \
       text not null, ADD COLUMN description text not null";
    ]
    |> List.map (T.unit ->. T.unit)

  let rollback =
    [
      "DROP TABLE Event";
      "DROP TABLE RavenUser";
      "DROP TABLE dream_session";
      "DROP TABLE Admin";
      "ALTER TABLE Event RENAME COLUMN datetimeLocal TO datetime";
      "ALTER TABLE Event DROP COLUMN title, DROP COLUMN location, DROP COLUMN \
       description";
    ]
    |> List.map (T.unit ->. T.unit)
end

let get_db_version (module Db : DB) =
  let%lwt result = Db.exec Q.create_migrations_table_if_not_exist () in
  let%lwt () = Caqti_lwt.or_fail result in
  let%lwt result = Db.exec Q.insert_default_version () in
  let%lwt () = Caqti_lwt.or_fail result in
  let%lwt version = Db.find Q.get_db_version () in
  Result.map Int64.to_int version |> Caqti_lwt.or_fail

let rec drop l = function 0 -> l | n -> drop (List.tl l) (n - 1)

let upgrade_server (module Db : DB) =
  let rec run_migrations version migrations =
    match migrations with
    | migration :: migrations ->
        let%lwt result = Db.start () in
        let%lwt () = Caqti_lwt.or_fail result in
        let%lwt result = Db.exec migration () in
        let%lwt () = Caqti_lwt.or_fail result in
        let%lwt result = Db.exec Q.record_migration (version + 1) in
        let%lwt () = Caqti_lwt.or_fail result in
        let%lwt result = Db.commit () in
        let%lwt () = Caqti_lwt.or_fail result in
        run_migrations (version + 1) migrations
    | [] -> Lwt.return ()
  in
  let%lwt version = get_db_version (module Db) in
  drop Q.migrations version |> run_migrations version

let is_admin email (module Db : DB) =
  let%lwt result = Db.find Q.is_admin email in
  Caqti_lwt.or_fail result |> Lwt.map @@ ( <= ) 1 (* 1 <= x *)

let list_admins (module Db : DB) =
  Lwt.bind (Db.collect_list Q.list_admins ()) Caqti_lwt.or_fail

let add_admin email (module Db : DB) =
  Lwt.bind (Db.exec Q.add_admin email) Caqti_lwt.or_fail

let remove_admin email (module Db : DB) =
  Lwt.bind (Db.exec Q.remove_admin email) Caqti_lwt.or_fail

type event = {
  datetime_local : string;
  title : string;
  location : string;
  description : string;
}

type event_with_id = {
  id : int;
  datetime_local : string;
  title : string;
  location : string;
  description : string;
}

let add_event (event : event) (module Db : DB) =
  Lwt.bind
    (Db.exec Q.add_event
       (event.datetime_local, event.title, event.location, event.description))
    Caqti_lwt.or_fail

let list_events (module Db : DB) =
  Lwt.bind
    (Db.fold Q.list_events
       (fun (id, (datetime_local, location, title, description)) ->
         List.cons { id; datetime_local; title; location; description })
       () [])
    Caqti_lwt.or_fail

let remove_event event_id (module Db : DB) =
  Lwt.bind (Db.exec Q.remove_event event_id) Caqti_lwt.or_fail
